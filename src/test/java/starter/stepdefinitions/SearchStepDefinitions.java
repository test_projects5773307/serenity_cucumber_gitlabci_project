package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import starter.apiClients.ProductApiClient;
import starter.enums.ProductsKeywords;
import starter.objects.Product;
import starter.utils.Deserializator;
import starter.utils.EnumUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;


public class SearchStepDefinitions {
    private ProductApiClient apiClient = new ProductApiClient();
    private Response response;
    private List<Product> products = new ArrayList<>();

    @When("user searches the {string}")
    public void userCallsEndpoint(String product) {
        response = apiClient.getProducts(product);
        String responseJson = response.then().extract().asString();
        Deserializator deserializator = new Deserializator();
        products = deserializator.deserializeJSON(responseJson);
    }

    @When("user searches the non-existing {string}")
    public void userCallsNonExistingEndpoint(String product) {
        response = apiClient.getProducts(product);
    }


    @Then("user should see the results displayed for {string}")
    public void userSeesTheResultsDisplayedForProduct(String product) {
        restAssuredThat(response -> response.statusCode(200));
        ProductsKeywords enumAccordingToProduct = EnumUtils.getEnumByValueContainsEnumName(ProductsKeywords.class, product);
        List<String> productKeywords = enumAccordingToProduct.getValues();
        List<Product> titles = products.stream()
                .filter(p -> productKeywords.stream().noneMatch(keyword -> p.getTitle().toLowerCase().contains(keyword)))
                .collect(Collectors.toList());
        assertThat(titles.isEmpty())
                .as("Title doesn't contain the next word " + product + ". Products without requested word" + titles)
                .isTrue();
    }

    @Then("user should not see the results displayed for tablet")
    public void userDoesNotSeeResult() {
        restAssuredThat(response -> response.statusCode(404));
        restAssuredThat(response -> response.extract().path("title", "true"));
    }

    @Then("user should see the results with the following fields: {string}")
    public void userVerifiesFields(String s) throws NoSuchFieldException, IllegalAccessException {
        List<Product> productsWithEmptyFields = new ArrayList<>();
        String[] arr = s.split(",");
        for (Product product : products) {
            Class clazz = product.getClass();
            for (String field : arr) {
                Field f = clazz.getDeclaredField(field);
                f.setAccessible(true);
                String valueOf = String.valueOf(f.get(product));
                f.setAccessible(false);
                if(valueOf.isEmpty()){
                    productsWithEmptyFields.add(product);
                }
            }
        }
        assertThat(productsWithEmptyFields.isEmpty()).as("There are products with empty fields").isTrue();

    }

    @Then("user should see the price of products is greater than {double} €")
    public void userShouldSeeThePriceOfProductIsGreater(double arg) {
        List<Double> prises =
                products.stream().map(Product::getPrice).filter(price -> price < arg).collect(Collectors.toList());
        assertThat(prises.isEmpty())
                .as("There are products with price less than " + arg)
                .isTrue();
    }
}


