package starter.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ProductsKeywords {
    APPLE("apple"),
    ORANGE("orange", "sinaasappel", "sina"),
    PASTA("pasta"),
    COLA("cola");


    private final List<String> values;

    ProductsKeywords(String... values) {
        this.values = new ArrayList<>(values.length);
        this.values.addAll(Arrays.asList(values));
    }

    public List<String> getValues() {
        return values;
    }

}
